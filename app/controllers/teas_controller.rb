class TeasController < ApplicationController

	def index
    @teas = Tea.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @teas }
    end
  end

  def show
    @tea = Tea.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @tea }
    end
  end

  def new
    @tea = Tea.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @tea }
    end
  end

  def edit
    @tea = Tea.find(params[:id])
  end

  def create
    @tea = Tea.new(params[:tea])
    @tea.photo = params[:tea][:photo]
   

    respond_to do |format|
      if @tea.save
        format.html { redirect_to teas_path, notice: 'Cha was successfully created.' }
        format.json { render json: @tea, status: :created, location: @tea }
      else
        format.html { render action: "new" }
        format.json { render json: @tea.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @tea = Tea.find(params[:id])

    respond_to do |format|
      if @tea.update_attributes(params[:tea])
        format.html { redirect_to @tea, notice: 'Cha was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @tea.errors, status: :unprocessable_entity }
      end
    end
  end


  def destroy
    @tea = Tea.find(params[:id])
    @tea.destroy

    respond_to do |format|
      format.html { redirect_to teas_url }
      format.json { head :no_content }
    end
  end


end
