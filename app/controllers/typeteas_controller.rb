class TypeteasController < ApplicationController
  # GET /typeteas
  # GET /typeteas.json
  def index
    @typeteas = Typetea.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @typeteas }
    end
  end

  # GET /typeteas/1
  # GET /typeteas/1.json
  def show
    @typetea = Typetea.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @typetea }
    end
  end

  # GET /typeteas/new
  # GET /typeteas/new.json
  def new
    @typetea = Typetea.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @typetea }
    end
  end

  # GET /typeteas/1/edit
  def edit
    @typetea = Typetea.find(params[:id])
  end

  # POST /typeteas
  # POST /typeteas.json
  def create
    @typetea = Typetea.new(params[:typetea])

    respond_to do |format|
      if @typetea.save
        format.html { redirect_to new_tea_path(@tea), notice: 'Typetea was successfully created.' }
        format.json { render json: @typetea, status: :created, location: @typetea }
      else
        format.html { render action: "new" }
        format.json { render json: @typetea.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /typeteas/1
  # PUT /typeteas/1.json
  def update
    @typetea = Typetea.find(params[:id])

    respond_to do |format|
      if @typetea.update_attributes(params[:typetea])
        format.html { redirect_to @typetea, notice: 'Typetea was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @typetea.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /typeteas/1
  # DELETE /typeteas/1.json
  def destroy
    @typetea = Typetea.find(params[:id])
    @typetea.destroy

    respond_to do |format|
      format.html { redirect_to typeteas_url }
      format.json { head :no_content }
    end
  end
end
