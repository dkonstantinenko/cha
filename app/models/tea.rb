class Tea < ActiveRecord::Base
  attr_accessible :hanzi_name, :harvest_id, :place_id, :presence, :price, :rus_name, :transliter_name, :typetea_id, :photo
  belongs_to :typetea
  belongs_to :harvest
  belongs_to :place

  has_attached_file :photo, :styles => { :small => "150x150" },
  											:storage => :dropbox,
										    :dropbox_credentials => Rails.root.join("config/dropbox.yml")										
 	 
end
