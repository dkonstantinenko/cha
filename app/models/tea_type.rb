class TeaType < ActiveRecord::Base
  attr_accessible :name
  has_many :teas, dependent: :destroy
end
