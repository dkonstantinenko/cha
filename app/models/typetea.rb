class Typetea < ActiveRecord::Base
  attr_accessible :name
  has_many :teas
end
