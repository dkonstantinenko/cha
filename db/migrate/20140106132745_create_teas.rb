class CreateTeas < ActiveRecord::Migration
  def change
    create_table :teas do |t|
      t.string :hanzi_name
      t.string :transliter_name
      t.string :rus_name
      t.integer :price
      t.integer :type_tea_id
      t.boolean :presence
      t.integer :place_id
      t.integer :harvest_year_id

      t.timestamps
    end
  end
end
