class AddHarvestIdToTeas < ActiveRecord::Migration
  def change
    add_column :teas, :harvest_id, :integer
  end
end
