class CreateHarvests < ActiveRecord::Migration
  def change
    create_table :harvests do |t|
      t.string :name

      t.timestamps
    end
  end
end
