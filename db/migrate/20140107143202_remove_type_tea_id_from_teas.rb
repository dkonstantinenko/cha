class RemoveTypeTeaIdFromTeas < ActiveRecord::Migration
  def up
    remove_column :teas, :type_tea_id
  end

  def down
    add_column :teas, :type_tea_id, :integer
  end
end
