class CreateHarvestYears < ActiveRecord::Migration
  def change
    create_table :harvest_years do |t|
      t.integer :name

      t.timestamps
    end
  end
end
