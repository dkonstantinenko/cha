class RemoveHarvestYearIdFromTeas < ActiveRecord::Migration
  def up
    remove_column :teas, :harvest_year_id
  end

  def down
    add_column :teas, :harvest_year_id, :integer
  end
end
