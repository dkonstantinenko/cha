class AddTypeteaIdToTeas < ActiveRecord::Migration
  def change
    add_column :teas, :typetea_id, :integer
  end
end
