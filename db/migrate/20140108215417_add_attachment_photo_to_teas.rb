class AddAttachmentPhotoToTeas < ActiveRecord::Migration
  def self.up
    add_attachment :teas, :photo
    end
  end

  def self.down
    remove_attachment :teas, :photo
  end
end
