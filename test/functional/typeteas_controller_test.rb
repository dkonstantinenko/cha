require 'test_helper'

class TypeteasControllerTest < ActionController::TestCase
  setup do
    @typetea = typeteas(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:typeteas)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create typetea" do
    assert_difference('Typetea.count') do
      post :create, typetea: { name: @typetea.name }
    end

    assert_redirected_to typetea_path(assigns(:typetea))
  end

  test "should show typetea" do
    get :show, id: @typetea
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @typetea
    assert_response :success
  end

  test "should update typetea" do
    put :update, id: @typetea, typetea: { name: @typetea.name }
    assert_redirected_to typetea_path(assigns(:typetea))
  end

  test "should destroy typetea" do
    assert_difference('Typetea.count', -1) do
      delete :destroy, id: @typetea
    end

    assert_redirected_to typeteas_path
  end
end
